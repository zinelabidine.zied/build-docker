FROM mcr.microsoft.com/dotnet/sdk:5.0-windowsservercore-ltsc2019

USER ContainerAdministrator

RUN dotnet tool install --global dotnet-sonarscanner --version 5.4.1

COPY SonarQube.Analysis.xml "C:\Users\ContainerAdministrator\.dotnet\tools\.store\dotnet-sonarscanner\5.4.1\dotnet-sonarscanner\5.4.1\tools\netcoreapp3.0\any\SonarQube.Analysis.xml"

COPY "openjdk-12+32_windows-x64_bin" "C:\Users\ContainerAdministrator\openjdk-12+32_windows-x64_bin"

